# README #

Start project notes


### DATABASE ###

* Create database phalcon_test
* Apply migrations for create table users / 'phalcon migration run' command, or see next step

### TEST USERS ###

* Next you must create users for test login
* First way: on your browser open url {project}/start , where {project} is your local domain
* Another way its import users from users.sql dump, which is located on root project directory

### FINALLY ###
* After those steps you get two users with roles admin and moderator and different access level
* Admin     - email: admin@mail.com     , password: admin1234
* Moderator - email: moderator@mail.com , password: mode1234

