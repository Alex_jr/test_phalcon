<?php

use Phalcon\Mvc\Controller;
use Phalcon\Acl\Enum as Acl;
use Phalcon\Events\Event;
use Phalcon\Acl\Role;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Component as Resource;

class ControllerBase extends Controller
{
    public $role;

    /**
     * @return bool
     */
    public function beforeExecuteRoute()
    {
        $auth = $this->session->get('auth');

        if (!$auth) {
            $role = 'guest';
        } else {
            $role = $auth['role'];
        }

        $this->view->role = $role;
        $this->view->user = $auth;

        $this->view->menuKey = $controller = $this->dispatcher->getControllerName();
        $action = $this->dispatcher->getActionName();

        if ($controller == 'session' && ($action == 'index' || $action == 'start')) {
            return true;
        }

        $acl = $this->getAcl();

        $allowed = $acl->isAllowed($role, $controller, $action);

        $this->view->logged = isset($auth) ? true : false;

        if (!$allowed) {
            if($auth['id']){
                $this->flash->error(
                    "You don't have access to Danger page"
                );
                $this->response->redirect('/');
            }
            else{
                $this->response->redirect('/session');
            }

            return false;
        }

        return true;
    }

    /**
     * @return AclList
     */
    protected function getAcl()
    {
        // Create the ACL
        $acl = new AclList();

        $acl->setDefaultAction(
            Acl::DENY
        );

        $roles = [
            'admin' => new Role('admin'),
            'moderator' => new Role('moderator'),
            'guest' => new Role('guest'),
        ];

        foreach ($roles as $role) {
            $acl->addRole($role);
        }

        $privateResources = [
            'danger'  => ['index']
        ];

        foreach ($privateResources as $resourceName => $actions) {
            $acl->addComponent(
                new Resource($resourceName),
                $actions
            );
        }

        $publicResources = [
            'index'  => ['index'],
            'session' => ['index', 'start', 'logout'],
            'start' => ['index']
        ];

        foreach ($publicResources as $resourceName => $actions) {
            $acl->addComponent(
                new Resource($resourceName),
                $actions
            );
        }

        $acl->allow(
            'guest',
            'start',
            'index'
        );

        foreach ($publicResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow(
                    'moderator',
                    $resource,
                    $action
                );

                $acl->allow(
                    'admin',
                    $resource,
                    $action
                );
            }
        }

        foreach ($privateResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow(
                    'admin',
                    $resource,
                    $action
                );
            }
        }

        return $acl;
    }
}
