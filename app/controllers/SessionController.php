<?php

use Phalcon\Mvc\View;
use Phalcon\Flash\Session as Flash;
use App\Models\Forms\LoginForm;
use Phalcon\Di;

class SessionController extends ControllerBase
{

    public function indexAction(){
        $this->view->logged = false;
        $form = new LoginForm();

        if ($this->request->isPost()) {
            if (false === $form->isValid($this->request->getPost())) {

                foreach ($form->getMessages() as $message) {
                    $this->flashSession->error($message);
                }
				//$this->view->disable();
                //$this->response->redirect('/session');
            }
            else{
                $email    = $this->request->getPost('email');
                $password = $this->request->getPost('password');

                $user = \Users::findFirst(
                    [
                        "email = :email:",// AND password = :password:",
                        'bind' => [
                            'email'    => $email,
                            //'password' => $password,
                        ]
                    ]
                );
                if($user){
                    if($user->password == sha1($password)){
                        $this->_registerSession($user);
                        return $this->response->redirect('/');
                    }
                    else{
                        $this->flashSession->error('Wrong password');
                    }
                }
                else{
                    $this->flashSession->error('User not found');
                }

                /*if ($user !== false) {
                    $this->_registerSession($user);
                    return $this->response->redirect('/');
                }*/
            }
            $this->response->redirect('/session');
        }

        $this->view->form = $form;
        //$this->response->redirect('/session');
    }

    private function _registerSession($user)
    {

        $this->session->set(
            'auth',
            [
                'id'   => $user->id,
                'name' => $user->name,
                'role' => $user->role,
            ]
        );
    }


    public function logoutAction()
    {
        $this->session->remove('auth');
        return $this->response->redirect('/session');
    }
}