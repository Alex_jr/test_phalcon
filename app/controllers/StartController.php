<?php

class StartController extends ControllerBase
{

    public function indexAction()
    {
        $check = false;
        $users = [
            [
                'name' => 'Admin',
                'email' => 'admin@mail.com',
                'password' => sha1('admin1234'),
                'role' => 'admin'
            ],
            [
                'name' => 'Moderator',
                'email' => 'moderator@mail.com',
                'password' => sha1('mode1234'),
                'role' => 'moderator'
            ]
        ];
        foreach($users as $user){
            if(!Users::findFirst(['email = :email:', 'bind' => ['email' => $user['email']]])){
                $userModel = new \Users();
                foreach($user as $param=>$value){
                    $userModel->$param = $value;
                }
                $userModel->save();
                $check = true;
                $this->flashSession->success('Test user '.$userModel->name .' successfully added!');
            }
        }
        if($check){
            //$this->response->redirect('/start');
        }
		else{
			$this->flashSession->success('Test users ready for use!');
		}
    }

}

