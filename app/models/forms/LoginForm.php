<?php

namespace App\Models\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;

class LoginForm extends Form
{
    public function initialize(){
        $email = new Email('email', ['class' => 'form-control']);
        $email->setFilters(
            'email'
        );
        $email->addValidators([
            new PresenceOf(
                [
                    'message' => 'Email field is required',
                ]
            ),
            new EmailValidator(
                [
                    'message' => 'The email is not valid',
                ]
            )
        ]);
        $this->add($email);

        $password = new Password('password', ['class' => 'form-control']);
        $password->addValidators([
            new StringLength(
                [
                    'min'            => 8,
                    'messageMinimum' => 'Password field is too short',
                ]
            )
        ]);
        $this->add($password);
    }
}
