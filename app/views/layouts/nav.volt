{% if logged %}
<nav class="navbar navbar-expand-lg navbar-light bg-light main-nav">
	<a class="navbar-brand" href="/"><i class="fas fa-fire-alt"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse text-center" id="navbarNavDropdown">
		<ul class="navbar-nav m-auto">
			<li class="nav-item active">
				<a class="nav-link" href="/">Dashboard <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link danger-link" href="/danger"><i class="fas fa-skull-crossbones"></i> Danger</a>
			</li>
			<li class="nav-item dropdown user-dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				{{ user['name'] }}
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item" href="/session/logout">Log out</a>
				</div>
			</li>
		</ul>
	</div>
</nav>
{% endif %}