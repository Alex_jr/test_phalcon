<div class="alert-block col-3 float-left">{{ flash.output() }}</div>
<div class="col-4 login-form">
	<p class="text-center"><b><big>Sign In</big></b></p>
	<form action="/session" method="post">
		<div class="form-group">
			<label for="username">Email</label>
			{{ form.render('email') }}
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			{{ form.render('password') }}
		</div>
		<div class="text-center">
			{{ submit_button('Sign In', 'class':'btn btn-success') }}
		</div>
	</form>
</div>