<div class="col-4 m-auto mt-5 p-5 text-center">
	<div class="alert-block">{{ flash.output() }}</div>
	{{ link_to('/', '<i class="fas fa-home"></i> Go to Main page') }}
</div>